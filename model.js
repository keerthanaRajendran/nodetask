
'use strict';



const APPPATH = require('app-root-path');
const entity = require(APPPATH + '/entity/index').sequelize.models;
const sequelize = require(APPPATH + '/entity/index').sequelize;

const isEmailExists = (email) => {

    return entity.employee.count({ where: { 'email_id': email } }) 
    .then(count => { 
      if (count != 0) { 
        return true;
      } 
      return false; 
  
     })
}

const loginUser = (emailid) => {
    return entity.employee.findOne({where:{'email_id': emailid}})
    .then((user) => {
        return user;
    })
    .catch((err) => {
        console.log(err)
       return err;
    })
}


const getUser = (id) => {
   return entity.employee.findAll({
      where : { 'id': id}
    })
    .then((user) => {
       return user;
    })
    .catch((err)=> {
        return err;
    })
}

const insertUser = (firstname, lastname, mobile, emailid, password, accesstoken, usertype) => {
    
    let sample =  entity.employee.build({
        'first_name': firstname,
        'last_name': lastname,
        'mobile':   mobile,
        'email_id': emailid,
        'password' : password,
        'access_token' : accesstoken,
        'user_type' : usertype
    });

    return sample.save().then((response) => {
        return response;
     })

    .catch((err) => {
        return new Error(err);
    })


}
  
const userList = (offset, limit, utype) => {
    return entity.employee.findAll({
        'where' : { 'user_type' : { [sequelize.Op.ne] : 'Superadmin',[sequelize.Op.eq] : utype }},
        'offset': parseInt(offset), 
        'limit': parseInt(limit)
         })
        .then((user) => {
            return user;
         })
         .catch((err)=> {
             return err;
         })
}

const testList = (id) => {
    return entity.employee.findAll({
    where: [{'id': 42 }, {'id':43}]  
     })
     .then((success) => {
    return success;
     })
     .catch((error) => {
     return new Error(error);
     })

    } 

const updateUser = (id, firstname, lastname, mobile, emailid, password, accesstoken) => {
    return entity.employee.update({'first_name': firstname, 'last_name': lastname,
    'mobile':   mobile,
    'email_id': emailid,
    'password' : password,
    'access_token' : accesstoken },{ 
        where: {'id': id }
    })
    .then((success) => {
       return success;
    })
    .catch((error) => {
        return new Error(error);
    })

}

const deleteUser = (id) => {
   return entity.employee.destroy({ where : {'id':id}})
  .then((success) => {
        return success;
    })
    .catch((error) => {
        return err;
    })
}



module.exports = {

    loginUser : loginUser,
    getUser : getUser,
    insertUser : insertUser,
    updateUser : updateUser,
    deleteUser : deleteUser,
    isEmailExists : isEmailExists,
    userList : userList,
    testList : testList

}