/**
 *  ------------------------------------------------------------------------
 *  
 *  Server Initialization
 *  
 * */

'use strict';

const express = require('express');
const app = express();
const cors = require('cors');
const body_parser = require('body-parser');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
const passport = require('passport');
var config = require('./config');
var passportJWT = require('passport-jwt');
//const admin = require('./admin');
const routes = require('./routes');
const profile = require('./profile');
const APPPATH = require('app-root-path');
const entity = require(APPPATH + '/entity/index').sequelize.models;



app.use(cors());
app.use('/', profile);
app.use(body_parser.urlencoded({ extended: false }))
// parse application/json
app.use(body_parser.json());

// respond with "hello world" when a GET request is made to the homepag




 var ExtractJwt = passportJWT.ExtractJwt;
 var JwtStrategy = passportJWT.Strategy;
 var jwtOptions = {} 
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = config.constants.SECRET_KEY; 
var strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) { 
    console.log('payload received', jwt_payload); // usually this would be a database call: 
    entity.employee.findOne({where: {'id': jwt_payload.id}})
    .then(function(user) {
        if (user) {
            next(null, user.toJSON()); 
        } else { 
            next(null, false); 
        }
    });
t});
 
passport.use(strategy);


app.use(routes);




app.listen(3004);







