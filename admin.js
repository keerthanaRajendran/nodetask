/**
 *  ------------------------------------------------------------------------
 *  
 *  Admin middleware
 *  
 * */

'use strict';

const passport = require('passport');

const AdminMiddleWare = (req, res, next) => {
   if (req.body.id) {
       passport.authenticate('jwt', (err, user) => {
           //if (user.role_code === "A" && user.id === req.body.id) {
            if (user.id === parseInt(req.body.id)) {
               next()
           } else {
               res.status(401).send({ Status: false, StatusCode: 401, message: 'You are not authorized.Because Invalid token' })
           }
       })(req, res, next)
   } else {
       res.status(401).send({ Status: false, StatusCode: 401, message: 'valid user' })
   }
}

module.exports = AdminMiddleWare;