
/**
 *  ------------------------------------------------------------------------
 *  
 *  Routes
 *  
 * */


'use  strict';

const express = require('express');
const router = express.Router();
const controller = require('./controller');
const auth = require('./admin');
const passport = require('passport');




router.post('/login', controller.loginUser);
router.post('/insert_user',  controller.insertUser);
router.get('/get_user',  passport.authenticate('jwt', { session: false }), controller.getUser);
router.post('/update_user', passport.authenticate('jwt', { session: false }), controller.updateUser);
router.delete('/delete_user', passport.authenticate('jwt', { session: false }), controller.deleteUser);
router.post('/test', passport.authenticate('jwt', { session: false }), controller.test);
router.get('/user_list', controller.userList);
router.post('/test_list',function(req, res){
    Controller.testlist
  });

module.exports = router;