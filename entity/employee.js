/**
 *  ------------------------------------------------------------------------
 *  
 *  Table Definition - Employee
 *  
 * */

'use strict';

const Sequelize = require('sequelize');
const joi = require('joi');
module.exports = (sequelize, DataTypes) => {
    const employee = sequelize.define('employee', {
        id : { type: Sequelize.INTEGER, autoIncrement: true,primaryKey: true},
        first_name : Sequelize.STRING,
        last_name : Sequelize.STRING,
        mobile : { type : Sequelize.STRING },
        email_id : { type : Sequelize.STRING(100), validate : { isEmail: true } },
        password : Sequelize.STRING  ,
        access_token : Sequelize.STRING,
        user_type: Sequelize.STRING
        


    },
    {
        timestamps: false,  
        tableName: 'employee'
    });

    employee.associate = (models) => {
                
    }

    employee.sync({force: false});

    return employee;

}