


'use strict';

const model = require('./model');
const Joi = require('joi');
const jwt = require('jsonwebtoken');
var config = require('./config');


const loginUser = (req, res) => {
    
    console.log(req.body);
    if(req.body && req.body.emailid && req.body.password) {
        let emailid = req.body.emailid;
        let password = req.body.password;

        model.loginUser(emailid)
        .then((user)=>{
         console.log("user  ",user)
            if( ! user ) {
                res.status(401).json({code : 1, message:"no such user found!"});
            } 

            if(user.password === req.body.password) {
                let payload = {id: user.id};
                let token = jwt.sign(payload, config.constants.SECRET_KEY );
                res.json({message: "ok", token: token});
            } else {
                res.status(401).json({message:"passwords did not match!"});
            }
        })
        .catch((err)=>{
            console.log(err)
            res.status(400).json({ message : 'Error!'})
        })

    } else {
        res.status(400).json({message: "Invalid Request!"});
    }

}

const getUser = (req, res) => {
    
    if(req.query.id) {
        model.getUser(req.query.id)
        .then((user)=>{
            res.status(200).json({code : 0, message: 'User', data : user});
        })
        .catch((err)=>{
            res.status(400).json({ code : 1, message : 'Error!'});
        })
    } else {
        res.status(400).json({ code : 1, message : 'Invalid Request!'})
    }

}

const insertUser = (req, res) => {


    let data = req.body;
    const schema = Joi.object().keys({
      firstname: Joi.string().min(3).max(20).required(),
      lastname: Joi.string().min(4).max(20).required(),
      mobile: Joi.string().min(10).max(10),
      birthyear: Joi.number().integer().min(1900).max(2013),
      password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
      accesstoken: Joi.string().min(2).max(30),
      emailid: Joi.string().email({ minDomainAtoms: 2 }),
      usertype: Joi.string().min(4).max(10)
   }).with('firstname', 'lastname').without('password', 'access_token');
   
   
   // You can also pass a callback which will be called synchronously with the validation result.
   Joi.validate(data, schema,  (err, value) => {  // err === null -> valid
             
        if(err) {
            res.status(422).json({
                code: 2,  
                message: err
            });
        } else {
            model.isEmailExists(req.body.emailid)
            .then(isExists => { 

                if (!isExists) { 
        
                    console.log(req.body)
                    model.insertUser(req.body.firstname, req.body.lastname, req.body.mobile, req.body.emailid, req.body.password, req.body.accesstoken, req.body.usertype)
                    .then((success)=> {
                        console.log(success)
                        res.json({ code : 0, message : 'Data inserted successfully!'});
                    })
                    .catch((err)=>{
                        res.json({ code : 1, message : err});
                    })
        
                } else {
        
                res.json({ code : 1, message : 'Email Already exists!'});
        
                }
        
            });
                        
        }
    })
    

}

const userList = (req, res) => {
    model.userList(req.query.offset, req.query.limit, req.query.usertype)
    .then((user)=>{
        console.log(user);
        res.status(200).json({code : 0, message: 'view userlist!', data: user});
    })
    .catch((err)=>{
        res.status(400).json({ code : 1, message : 'Error!'});
    })

}
 const testList = (req,res) => {
     model.testList(req.body.id)
     .then((user)=>{
         res.status(200).json({code : 0, message: 'yes', data: user});
     })
     .catch((err) => {
         res.status(400).json({code :1, message: 'no',});

     })
 }


const updateUser = (req, res) => {
    console.log(req.body)
    model.updateUser(req.body.id, req.body.firstname, req.body.lastname, req.body.mobile, req.body.emailid, req.body.password, req.body.accesstoken )
    .then((user)=>{
        console.log(user);
         res.status(200).json({code : 0, message: 'Updated!'});
    })
    .catch((err)=>{
        res.status(400).json({ code : 1, message : 'Error!'});
    })

}

const deleteUser = (req, res) => {

  let id = req.body.id;
  model.deleteUser(id)
  .then((success) => {
        res.json({code : 0, message : 'Deleted!'});
   })
   .catch((error) => {
       res.json({code : 1, message : 'Error!'});
   })
}

const test = (req, res) => {
    
    let data = req.body;
    const schema = Joi.object().keys({
      firstname: Joi.string().min(3).max(20).required(),
      lastname: Joi.string().min(4).max(20).required(),
      birthyear: Joi.number().integer().min(1900).max(2013),
      password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
      accesstoken: Joi.string().token(),
      emailid: Joi.string().email({ minDomainAtoms: 2 })
   }).with('firstname', 'lastname').without('password', 'access_token');
   
   
   // You can also pass a callback which will be called synchronously with the validation result.
   Joi.validate(data, schema, function (err, value) {   // err === null -> valid
           
        const id = Math.ceil(Math.random() * 9999999);
  
        if(err) {
            res.status(422).json({
                status: 'error',
                message: 'Invalid request data',
                data: data
            });
        } else {
            res.json({
                status: 'success',
                message: 'User created successfully',
                data: Object.assign({id}, value)
            });      
        }
    })
}
 

module.exports = {
    loginUser : loginUser,
    getUser : getUser,
    insertUser : insertUser,
    updateUser : updateUser,
    deleteUser : deleteUser,
    test : test,
    userList : userList,
    testList : testList
}